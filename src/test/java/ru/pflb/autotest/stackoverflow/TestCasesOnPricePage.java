package ru.pflb.autotest.stackoverflow;

import org.testng.Assert;

import org.testng.annotations.Test;
import ru.pflb.autotest.stackoverflow.pages.PricesPage;

public class TestCasesOnPricePage {
    PricesPage pricesPage = new PricesPage();

    @Test
    public void test1 ()
    {
        pricesPage.open();
        pricesPage.sendKeysToSearchFieldAndSubmit("Подписка о не выезде");
        boolean b = false;
        b = pricesPage.checkForShownText();
        Assert.assertEquals(b, true);
    }

    @Test
    public void test2 ()
    {
        pricesPage.open();
        pricesPage.clickOnLoginButton();
        pricesPage.clickOnResetPasswordTab();
        boolean b = false;
        b = pricesPage.checkForResetPasswordTitle();
        Assert.assertEquals(b, true);
    }

}
