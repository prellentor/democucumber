package ru.pflb.autotest.stackoverflow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class TestCase {
    @Test
    public void test(){
        System.setProperty("webdriver.chrome.driver", "D:\\AT_S\\NSK 2019.05 #2\\classwork\\stackoverflow\\bin\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        //параметр для отключения инфо-бара "Хромо управляет автоматизир. ПО"
        options.addArguments("disable-infobars");

        ChromeDriver driver = new ChromeDriver(options);

        //развернуть окно браузера на весь экран
        driver.manage().window().maximize();
        driver.get("http://www.stackoverflow.com");
        //driver.close(); //закрывает текущую вкладку

        String input = "why singleton is bad";

        String searchFieldXpath = "//input[@name='q']";

        //WebElement searchField = driver.findElement(By.xpath(searchFieldXpath));
        WebElement searchField = driver.findElementByXPath(searchFieldXpath);

        searchField.sendKeys(input);

        String searchButtonXpath = "//button[@aria-label='Search…']";
        WebElement searchButton = driver.findElementByXPath(searchButtonXpath);
        searchButton.click();

        String firstResultXpath = "//a[@class='question-hyperlink'][1]";
        WebElement firstResult = driver.findElementByXPath(firstResultXpath);
        firstResult.click();
        //driver.quit(); //закрывает все вкладки и инстанс хрома

    }
}
