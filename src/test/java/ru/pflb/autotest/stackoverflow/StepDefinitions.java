package ru.pflb.autotest.stackoverflow;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import ru.pflb.autotest.stackoverflow.pages.MainPage;
import ru.pflb.autotest.stackoverflow.pages.SearchResultPage;

public class StepDefinitions {

    MainPage mainPage = new MainPage();
    SearchResultPage searchResultPage = new SearchResultPage();

    @Дано("открыта главная страница stackoverflow.com")
    public void открыта_главная_страница_stackoverflow_com() {
        mainPage.open();
    }

    @Если("в поисковую строку введен запрос {string}")
    public void в_поисковую_строку_введен_запрос(String string) {
        mainPage.setSearchField(string);
    }

    @Если("нажата кнопка поиска")
    public void нажата_кнопка_поиска() {
        mainPage.clickSearchButton();
    }

    @Тогда("мы можем кликнуть по третьему результату поисковой выдачи")
    public void мы_можем_кликнуть_по_третьему_результату_поисковой_выдачи() {
        searchResultPage.dismissCookieBanner();
        searchResultPage.clickSearchResultElement(3);
    }
}
