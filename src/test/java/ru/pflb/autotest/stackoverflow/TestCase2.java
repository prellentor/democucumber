package ru.pflb.autotest.stackoverflow;

import org.testng.annotations.Test;
import ru.pflb.autotest.stackoverflow.pages.MainPage;
import ru.pflb.autotest.stackoverflow.pages.SearchResultPage;

public class TestCase2 {

    MainPage mainPage = new MainPage();
    SearchResultPage searchResultPage = new SearchResultPage();

    @Test
    public void test(){
        mainPage.open();
        mainPage.setSearchField("why singleton is bad");
        mainPage.clickSearchButton();
        searchResultPage.dismissCookieBanner();
        searchResultPage.clickSearchResultElement(3);
    }
}
