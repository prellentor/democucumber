package ru.pflb.autotest.stackoverflow.pages;

import org.openqa.selenium.WebElement;

public class SearchResultPage extends BasePage{
    public void clickSearchResultElement(int num){
        String resultXpathTemplate = "//div[%s]//a[@class='question-hyperlink']";
        String resultXpath = String.format(resultXpathTemplate, num);
        WebElement firstResult = driver.findElementByXPath(resultXpath);
        firstResult.click();
    }
    public void dismissCookieBanner(){
        String cookieBannerXpath = "//a[@aria-label='notice-dismiss']";
        WebElement cookieBanner = driver.findElementByXPath(cookieBannerXpath);
        cookieBanner.click();
    }
}
