package ru.pflb.autotest.stackoverflow.pages;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.Keys;

public class PricesPage extends BasePage {

    public void open()
    {    driver.get("http://javarush.ru/prices");   }

    public void checkDayNight ()
    {
        WebElement dayNightCheckBox = driver.findElementByXPath("//div[@class='switcher-daynight']/input");
        dayNightCheckBox.click();
    }

    public void clickOnLoginButton()
    {
        WebElement loginButton = driver.findElementByXPath("//a[@href=\"/login/signup\"]");
        loginButton.click();
    }

    public void clickOnPremiumButton()
    {
        WebElement button = driver.findElementByXPath("//a[@href=\"/prices/buy?key=JR20_PREMIUM\"]/..");
        button.click();
    }

    public void clickOnResetPasswordTab ()
    {
        WebElement resetpasswordTab = driver.findElementByXPath("//span[contains(text(),\"Восстановление пароля\")]");
        resetpasswordTab.click();
    }

    public boolean checkForEnterText()
    {
        WebElement element = driver.findElementByXPath("//div[@class=\"auth-widget\"]/h2[text()=\"Вход\"]");
        if (element != null)
            return true;
        return false;
    }

    public void clickOnSearchButton()
    {
        WebElement searchButton = driver.findElementByXPath("//input[@type=\"search\"]");
        searchButton.click();
    }

    public void sendKeysToSearchFieldAndSubmit (String str)
    {
        WebElement searchField = driver.findElementByXPath("//input[@type=\"search\"]");
        searchField.sendKeys(str);
        searchField.sendKeys(Keys.RETURN);
    }

    public boolean checkForShownText ()
    {
        WebElement searchField = driver.findElementByXPath("//div[contains(text(),\"показано\")]");
        if (searchField != null)
            return true;
        return false;
    }

    public boolean checkForResetPasswordTitle () {
        WebElement searchField = driver.findElementByXPath("//h2[contains(text(),\"Восстановление пароля\")]");
        if (searchField != null)
            return true;
        return false;
    }
}
